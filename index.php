<?php get_header(); /* div#main is opened in header.php */ ?>

  <?php if ( have_posts() ) : ?>
  <div id="content">
    <?php while ( have_posts() ) : the_post(); ?>
      <a href="<?php the_permalink(); ?>"><h2 class="item-title"><?php the_title(); ?></h2></a>
      <?php the_excerpt(); ?>
      <a class="readmore-link" href="<?php the_permalink(); ?>">Read More &rarr;</a>

      <div class="entry-meta">
        <div class="post-date"><?php the_date(); ?></div>

        <?php $categories_list = get_the_category_list( ', ' ); ?>
        <?php if ( $categories_list ): ?>
          <div class="cat-links"> Posted in: <?php echo $categories_list; ?> </div>
        <?php endif; // End if categories ?>

        <?php $tags_list = get_the_tag_list( '', ', ' ); ?>
        <?php if ( $tags_list ): ?>
          <div class="tag-links"> Tagged: <?php echo $tags_list; ?> </div>
        <?php endif; // End if $tags_list ?>
      </div><!-- #entry-meta -->
    <?php endwhile; ?>
  </div><!--/#content-->
  <?php else: ?>
    <h2 class="page-title">Not Found</h2>
    <div id="content">
      <p>Sorry, the item you requested could not be found.</p>
    </div><!--/#content-->
  <?php endif; ?>

  <?php get_sidebar(); ?>

<?php get_footer(); /* div#main is closed in footer.php */ ?>