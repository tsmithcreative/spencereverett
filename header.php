<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="gt-ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="gt-ie8"> <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />

    <title><?php echo tsmith_make_title(); ?></title>

    <link href='http://fonts.googleapis.com/css?family=Quicksand:400,700' rel='stylesheet' type='text/css' />

    <?php // Include the html5shiv for old versions of Internet Explorer ?>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script>window.html5 || document.write('<script src="js/html5shiv.js"><\/script>')</script>
    <![endif]-->

    <?php wp_head(); ?>
  </head>
  <body <?php body_class($class); ?> >
  <div id="container">
    <nav id="nav1">
      <ul><?php print spencereverett_get_primary_menu(); ?></ul>
    </nav>
    <header id="headline">
      <a href="<?php bloginfo('url')?>">
        <h1>Everett Creative</h1>
      </a>
    </header>
    <div id="main">