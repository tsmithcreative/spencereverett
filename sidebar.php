<div id="sidebar">
  <nav id="nav2">
    <ul>
      <?php
        if ( is_singular( 'project' ) ) { 

          // It's a Project. Get its "artmedia" term and list all projects therein
          global $post;
          $terms = wp_get_post_terms( $post->ID, 'artmedia', array('fields' => 'ids'));

          $projects = new WP_Query(array(
            'post_type'    => 'project',
            'orderby'      => 'title',
            'tax_query'    => array(
              array(
                'taxonomy'   => 'artmedia',
                'field'      => 'id',
                'terms'      => $terms,
                'operator'   => 'IN'
              ),
            ),
          ));

         $current_project = $post->ID;

          if ( $projects->have_posts() ) : while ( $projects->have_posts() ) : $projects->the_post();
            $class = ( $current_project == $post->ID ) ? "current_page_item" : '';
            ?>
              <li class="<?php echo $class; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php
          endwhile; endif;
          wp_reset_postdata();
        }else if ( is_singular() ) {

          // It's a page, post, or custom post type post; list pages
          wp_list_pages('title_li=&echo=1');

        }
      ?>
    </ul>
  </nav>
</div><!--/#sidebar-->