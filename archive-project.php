<?php get_header(); /* div#main is opened in header.php */ ?>

<?php /* This list code would be used for a taxonomy archive */ ?>
<?php if ( have_posts() && function_exists('slt_cf_field_value') ) : ?>
  <ul class="thumbnails">
    <?php while ( have_posts() ) : the_post(); ?>
    <li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php
        // Get thumbnails
        $mono = slt_cf_field_value('thumb_mono');
        $full = slt_cf_field_value('thumb_full');

        $mono = ($mono) ? esc_url(wp_get_attachment_url($mono)) : false;
        $full = ($full) ? esc_url(wp_get_attachment_url($full)) : false;
      ?>

      <img src="<?php echo $mono; ?>" alt="Project 1" class="default" height="200" width="200">
      <img src="<?php echo $full; ?>" alt="" class="active" height="200" width="200" />
    </a></li>
    <?php endwhile; ?>
  </ul>
  <?php else: ?>
    <h2 class="not-found">No projects found.</h2>
  <?php endif; ?>
<?php /* End of taxonomy list */ ?>

<?php get_footer(); /* div#main is closed in footer.php */ ?>