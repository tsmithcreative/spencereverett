<?php get_header(); /* div#main is opened in header.php */ ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <h2 class="page-title"><?php the_title(); ?></h2>
    <div id="content">
      <?php the_content(); ?>
      <?php wp_link_pages(); ?>
      <?php edit_post_link('Edit This Post', '<p class="edit-link">', '</p>'); ?>
    </div><!--/#content-->
  <?php endwhile; else: ?>
    <h2 class="page-title">Not Found</h2>
    <div id="content">
      <p>Sorry, the item you requested could not be found.</p>
    </div><!--/#content-->
  <?php endif; ?>

  <?php get_sidebar(); ?>

<?php get_footer(); /* div#main is closed in footer.php */ ?>