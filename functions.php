<?php

/**
 * Inlcude tsmith512's WordPress utility and cleaning functions
 */
include('functions-tsmith.php');

/**
 * Include the setup for custom post types (project) and its taxonomy
 */
include('functions-cpt.php');

/**
 * spencereverett_enqueue_scripts() runs on wp_enqueue_scripts to add
 * theme-specific CSS and JS. Bail if we're in admin.
 */
function spencereverett_enqueue_scripts() {
  if ( is_admin() ) return;

  // The primary stylesheet
  $spencereverett_styles = (get_stylesheet_directory_uri() . '/css/main.css');
  wp_enqueue_style('spencereverett_styles', $spencereverett_styles);

  // The colorbox stylesheet
  $spencereverett_cb_styles = (get_stylesheet_directory_uri() . '/css/colorbox.css');
  wp_enqueue_style('spencereverett_cb_styles', $spencereverett_cb_styles);

  // Modernizr: load in head to prevent the flash of unstyled content
  $spencereverett_modernizr = (get_stylesheet_directory_uri() . '/js/modernizr-custom.min.js');
  wp_enqueue_script('spencereverett_modernizr', $spencereverett_modernizr, null, null, false );

  // Colorbox: for gallery images
  $spencereverett_colorbox = (get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js');
  wp_enqueue_script('spencereverett_colorbox', $spencereverett_colorbox, array( 'jquery' ), null, true );

  // The theme-specific javascript
  $spencereverett_scripts = (get_stylesheet_directory_uri() . '/js/main.js');
  wp_enqueue_script('spencereverett_scripts', $spencereverett_scripts, array( 'jquery' ), null, true );

  // Clear out the crap that comes from SLT's Developer's Custom Fields. We don't
  // need any of this on the front-end.
  wp_dequeue_script('slt-cf-scripts');
  wp_dequeue_script('jquery-ui-datepicker');
  wp_dequeue_script('slt-cf-file-select');
  wp_dequeue_script('google-maps-api');
  wp_dequeue_script('slt-cf-gmaps');
}
add_action('wp_enqueue_scripts','spencereverett_enqueue_scripts');

/**
 * Setup Custom Fields using 'Developer's Custom Fields' by Steve Taylor:
 * http://wordpress.org/extend/plugins/developers-custom-fields/
 */
add_action('init', 'spencereverett_dcf');
function spencereverett_dcf() {
  /* If the plugin isn't enabled, skip out. This way we don't WSOD in problems... */
  if ( !function_exists('slt_cf_setting') || !function_exists('slt_cf_register_box') ) return false;

  define(SLT_CF_USE_GMAPS, 'false'); /* We aren't using the Google Maps stuff */

  slt_cf_register_box(array(
    'type' => 'post',
    'title' => 'Project Thumbnails',
    'id' => 'spencereverett_project_meta_thumbnails',
    'description' => 'Matching monochrome and full-color thumbnails for archive lists.',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
      array(
        'name' => 'thumb_mono',
        'label' => 'Monochrome Thumbnail',
        'type' => 'file',
        'scope' => array('project'),
        'description' => ('Monochrome thumbnail, either transparent PNG or using ' .
          '#F2DEA0 (site background color) as a main background. The full-color  ' .
          'thumbnail will fade-in on top of it. Size should be exactly 200x200.'),
        'capabilities' => array( 'edit_posts' ),
      ),
      array(
        'name' => 'thumb_full',
        'label' => 'Full-Color Thumbnail',
        'type' => 'file',
        'scope' => array('project'),
        'description' => ('Full-color thumbnail, which will fade-in on over the ' .
          'monochrome thumbnail on hover. Size should be exactly 200x200.'),
        'capabilities' => array( 'edit_posts' ),
      ),
    ),
  ));
}

/**
 * Declare the Primary Nav Menu
 */
function spencereverett_register_custom_menus() {
  register_nav_menus(array(
    'PrimaryMenu' => __('Primary Menu'),
  ));
}
add_action('init', 'spencereverett_register_custom_menus');

/**
 * Quick function to call the Primary Menu with preferred default options
 */
function spencereverett_get_primary_menu() {
  $PrimaryMenu_options = array(
    'theme_location'  => 'PrimaryMenu',
    'container'       => false, 
    'echo'            => false,
    'fallback_cb'     => false,
    'items_wrap'      => '%3$s',
    'depth'           => 0,
  );
  return wp_nav_menu( $PrimaryMenu_options );
}

/**
 * Alter body classes
 */
function spencereverett_body_classes( $classes ) {
  global $post, $template;

  // Are we doing a thumbnails display for a Projects archive or 
  if ( is_post_type_archive( 'project' ) || is_tax( 'artmedia' ) ) {
    $classes[] = "thumbnails-page";
  }

  return $classes;
}
add_filter('body_class','spencereverett_body_classes');
